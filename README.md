# Configuring A Droplet For Recon

This guide might not be for everyone and might not work for everyone, but it is what worked for me. Some of the dependencies listed may not be required, but I went ahead and added them anyway, since there are quite a few tools that require them.

I'll be using Nahamsec's lazy recon setup for this, as he has got a really great setup with most of the tools needed for recon. Plus he already has a bash profile and install script ready to go on his github.

I opted for Debian on my droplet, but the commands will also work for Ubuntu. It's possible, however, that some of the dependencies may not be needed on Ubuntu. I'll leave that to someone else to try.

# Prep Work

First thing to do is update Debian.

```
$ apt update -y && apt upgrade -y
```

Once you have updated, then you can go ahead and install everything below.

Dependencies to install:

- dnsutils

```
$ apt install -y dnsutils
```

- python

```
$ apt install -y python
```

- pip

```
$ apt install -y python-pip
```

- ruby

```
$ apt install -y ruby
```

- curl

```
$ apt install -y curl
```

- snapd
 
```
$ apt install -y snapd
```

- git

```
$ apt install -y git
```

Chromium snap package is needed for Aquatone to work.

Snap Packages:

- core

```
$ snap install core
```

- chromium

```
$ snap install chromium
```

# Setup

Now that we've laid the foundation, we can download the nahamsec repos.

```
$ git clone https://github.com/nahamsec/recon_profile
```

After you download the recon_profile, you can move the bash_profile it to your home directory and rename it to .bash_profile.

```
$ cd recon_profile
$ mv bash_profile ~
$ mv bash_profile .bash_profile
```


To start using the aliases in the profile, use the `source` (Luke) command.

```
$ source ~/.bash_profile
```

Next, we'll need to clone the Bug Bounty Hunting Tools repo.

```
$ git clone https://github.com/nahamsec/bbht
```

Then you'll want to make the shell script executable using `chmod` and run it.

```
$ cd bbht
$ chmod +x install.sh
$ ./install.sh
```


The script will install all the necessary tools and will also prompt you to install Go (which is why I didn't include it in the dependencies) which you will want to make sure and specify option 1 to install it.

Last thing you'll need to do is to make the Lazy Recon shell script executable so that you can run it.

```
$ cd ~/tools/lazyrecon/
$ chmod +x lazyrecon.sh
```


Now you can run `poweroff` to shut the droplet down and create a snapshot to deploy whenever you like.

That is all you should need to have a remote shell for recon and avoid getting your home IP blacklisted.

# Optional

A few optional things that you can do to make things a bit faster and easier.

- Remove password auth to use ssh keys only on droplet.
- Create an alias to tarball recon target directories on droplet.
- Create an alias for scp to transfer recon tarballs locally.

Here's the function that I added to my bash profile locally to make file transfer easier:

```
dlfile(){
scp -i ~/.ssh/id_rsa root@$1:~/tools/lazyrecon/$2 ~
}
```


All you would need to do is run `dlfile` with the IP address or hostname and the file as arguments to download your recon data.

Be sure to use this link below to get your Digital Ocean account set up so you can get $50 credit:

[DigitalOcean Referral](https://m.do.co/c/02fe046d66c9)

By doing so, you'll also be helping me out!

# Contact

Questions?

team[at]afslabs.net or info[at]saltwrx.org

# Thanks

Shout out to Ben for the awesome work he has done and for being a big contributor to the infosec and bug bounty communities.

Feel free to thank him yourself:

Twitch: [@nahamsec](https://twitch.tv/nahamsec)
Twitter: [@nahamsec](https://twitter.com/nahamsec)
Github: [@nahamsec](https://github.com/nahamsec)

This thanks list will be updated at a later time to include those who put in all the work to create the awesome tools we use for recon. I need a little break from typing all of this down for now.